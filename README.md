# I2P Browser

A Firefox configuration for I2P.

This is a Firefox configuration that is hardened for privacy and uses I2P. There is no official browser profile for I2P on Linux.

If using Windows I highly recommend to use the official Firefox profile https://geti2p.net/en/download/firefox

This is a work in progress. Do not use it yet.