#!/bin/bash -e

## Script for I2P Browser

## Checks if I2P is installed
# Package from https://geti2p.net
if [ ! -d /home/*/i2p ]; then

  # AUR package
  if [ ! -d /opt/i2p ]; then

    # i2pd
    if [ ! -d /etc/i2pd ]; then

      # Stops script
      echo 'I2P is not installed.'
      exit
    fi
  fi
fi

read -r -p "Start script? (y/n) " start
if [ "{start}" = "y" ]; then
  rm -rf /home/*/.mozilla/firefox/*.default
  cp -r /home/*/i2pbrowser/i2p.default /home/*/.mozilla/firefox/i2p.default
  rm /home/*/.mozilla/firefox/profiles.ini
  cp /home/*/i2pbrowser/profiles.ini /home/*/.mozilla/profiles.ini
fi
